Availability		
		
	HADR database disconnected	HADR
HADR database disconnected from all standby databases	HADR database disconnected at least one standby database	
	HADR database not ready for role switch	HADR
None of the HADR standby databases are ready for a role switch	One or more of the HADR standby databases are not ready for a role switch	
	HADR primary database blocked from writing to the log file	HADR
60000ms	30000ms	
	HADR standby database in replay-only window	HADR
Standby database is in a replay-only window		
	HADR standby log receiving falling behind log shipping from primary	HADR
2048MB	1024MB	
	Standby database log receive buffer full	HADR
The log receive buffer on the standby database is full, and the buffer will block the primary database from writing to the log.		
	Log space	Logging
95.00%	90.00%	
	Physical memory in use	Memory
95.00%	90.00%	
	Virtual memory in use	Memory
95.00%	90.00%	
	Cluster caching facility status	PureScale
STOPPED,ERROR,CF_NOT_FOUND	RESTARTING,CATCHUP	
	Cluster host status	PureScale
INACTIVE		
	PureScale member status	PureScale
STOPPED,ERROR	RESTARTING,WAITING_FOR_FAILBACK	
	Incomplete recovery	Recovery
RESTORE PENDING,ROLLFORWARD PENDING		
	Invalid log path	Recovery
DB2 error ADM1813E or DB2 error ADM1814E		
	Log archive failed	Recovery
Warning ADM1848W		
	Table invalid	Recovery
AVAILABLE is N		
	Table space backup pending	Recovery
BACKUP PENDING		
	Table space drop pending	Recovery
DROP PENDING		
	Database availability	Status
UNREACHABLE,QUIESCED	ROLLFORWARD,QUIESCE PENDING	
	Database partition availability	Status
OFFLINE		
	File system utilization	Storage
95.00%	90.00%	
	Table space container state	Storage
INACCESSIBLE		
	Table space container utilization	Storage
95.00%	90.00%	
	Table space quiesced	Storage
	QUIESCED	
	Table space state	Storage
OFFLINE		
	Table space utilization	Storage
95.00%	90.00%	
Performance		
		
	Connections	Connections
	150	100
	Buffer pool asynchronous read ratio	I/O
	90.00%	95.00%
	Buffer pool asynchronous write ratio	I/O
	90.00%	95.00%
	Buffer pool hit ratio	I/O
	90.00%	95.00%
	Buffer pool hit ratio degrades	I/O
		4x
	Catalog cache hit ratio	I/O
	60.00%	70.00%
	Application connections with excessive lock waits over request time	Locking
	10.00%	5.00%
	Increased table lock wait and lock timeout event hit rate.	Locking
5x	3x	
	Table deadlock occurred	Locking
One or more tables are in deadlock		
	Degree of secondary log allocation	Logging
95.00%	90.00%	
	Log space used by dirty pages	Logging
95.00%	90.00%	
	Package cache hit ratio	Memory
	70.00%	80.00%
	Group buffer pool hit ratio	PureScale
	90.00%	95.00%
	Total page reclaims per min	PureScale
1000	600	
	Virtual memory in use in CF system	PureScale
95.00%	90.00%	
	Maximum log space exceeded	Recovery
DB2 warning ADM1540W or DB2 warning ADM1542W		
	Post threshold sorts	Sorting
	10.00%	5.00%
	Sort overflows	Sorting
	10.00%	5.00%
	CPU time	Statements
	60000ms	10000ms
	Rows read	Statements
	2000000	1000000
	Rows returned	Statements
	2000000	1000000
	Statement performance	Statements
10x	5x	
	Time waited on locks	Statements
	150s	60s
	Total activity time	Statements
	300s	120s
	Total activity wait time	Statements
	150s	60s
	Database performance issue (technique preview)	Workload
	Threshold has been predefined	
	Rollback transactions	Workload
	5.00%	3.00%
	Rows read per fetched row	Workload
	10	5
Configuration		
		
	Log size	Diagnostic
	db2diag file size >= LOGFILSIZ	
	Notify level configuration parameter	Diagnostic
	NOTIFYLEVEL > 3	
	Diagnostic level	Diagnostic
	DIAGLEVEL = 4	
	Client I/O block size (rqrioblk)	I/O
	RQRIOBLK < 65535 bytes	
	Sequential detection (seqdetect)	I/O
	SEQDETECT = NO	
	Number of I/O servers configuration parameter	I/O
	NUM_IOSERVERS ≠ AUTOMATIC and SELF_TUNING_MEM ≠ OFF	
	Number of asynchronous page cleaners configuration	I/O
	NUM_IOCLEANERS ≠ AUTOMATIC and SELF_TUNING_MEM ≠ OFF	
	Changed pages threshold	I/O
	CHNGPGS_THRESH > 60% and the percentage of UPDATE / INSERT / MERGE / DELETE statements executed on the database is greater than 70%	
	Block on log disk full configuration	Logging
	BLK_LOG_DSK_FUL = NO	
	Log archmeth1 enable (logarchmeth1)	Logging
	LOGARCHMETH1 = OFF	
	Log space utilization	Logging
	Log space utilization > 75%	
	Log file size (logfilsiz)	Logging
	LOGFILSIZ < 2048	
	Sort heap threshold	Memory
	SHEAPTHRES < (10 x SORTHEAP)	
	Package cache size	Memory
	Package cache hit ratio < 80%	
	Package cache overflows	Memory
	PKG_CACHE_NUM_OVERFLOWS > 4	
	Sort memory not optimal	Memory
	(Sort overflows / Total sorts) > 5%	
	Utility impact limit (util_impact_lim)	Utility
	90% < UTIL_IMPACT_LIM < 100%	
	Automatic runstats collection	Utility
	AUTO_MAINT, AUTO_DB_BACKUP, and AUTO_TBL_MAINT is set to ON but AUTO_RUNSTATS = OFF	
	Automatic maintenance	Utility
	AUTO_MAINT = OFF	
	Workload management collection interval	Workload
	WLM_COLLECT_INT = 0	

