JSON Related
----------

* Docker Run for Db2
```
docker run -itd --name db2 --privileged=true -p 50000:50000 -e LICENSE=accept -e DB2INST1_PASSWORD=passDb2 ibmcom/db2:latest --cpus="3" --memory="8g"
```



* Create Database for JSON
```
db2 create database jsondb automatic storage yes using codeset utf-8 territory us pagesize 32 K
```

```
CREATE BUFFERPOOL NOSQLSYSTOOLSBP ALL DBPARTITIONNUMS SIZE AUTOMATIC PAGESIZE 32K
CREATE TEMPORARY TABLESPACE NOSQLSYSTOOLSTEMP PAGESIZE 32K MANAGED BY AUTOMATIC STORAGE BUFFERPOOL NOSQLSYSTOOLSBP
```

* Enable JSON Functionality
```
export PATH=$PATH:~/sqllib/java/jdk64/jre/bin/

cd ~/sqllib/json/bin

# ./db2nosql.sh -setup disable
./db2nosql.sh -setup enable

```

* Sample Example
```
./db2nosql.sh -user db2inst1 -hostName localhost -port 50000 -db jsondb -password passDb2 -setup enable
```